/*
 * Filename: CApp_OnRender.cc
 * Author: Denis Cormier
 * Date of creation: 3 September 2011
 * Description: Render images
 */

#include "CApp.h"
#include <string>
#include <sstream>

void CApp::OnRender() {
  CSurface::OnDraw(Surf_Display, Surf_Grid, 0, 0);
  
  for(int i = 0; i < 9; i++) {
    int X = (i % 3) * 110;
    int Y = (i / 3) * 110;

    if (Grid[i] == GRID_TYPE_X) {
      CSurface::OnDraw(Surf_Display, Surf_X, X, Y);
    }
    else if (Grid[i] == GRID_TYPE_O) {
      CSurface::OnDraw(Surf_Display, Surf_O, X, Y);
    }
  }
  Surf_Sidebar = TTF_RenderText_Solid(font, "Shroom-tac-toe!", textColor);
  CSurface::OnDraw(Surf_Display, Surf_Sidebar, 345, 0);
  Surf_Sidebar = TTF_RenderText_Solid(font, "Green     Red", textColor);
  CSurface::OnDraw(Surf_Display, Surf_Sidebar, 350, 50);

  std::stringstream gss;
  std::stringstream rss;
  std::stringstream minss;
  std::stringstream secss;
  std::stringstream milliss;
  std::string strg;
  
  gss << totalgreenwins;
  strg = gss.str();
  char green[strg.length() + 1];
  strcpy(green, strg.c_str());
  
  Surf_Sidebar = CSurface::OnLoad("images/black.bmp");
  CSurface::OnDraw(Surf_Display, Surf_Sidebar, 355, 75);
  Surf_Sidebar = TTF_RenderText_Solid(font, green, textColor);
  CSurface::OnDraw(Surf_Display, Surf_Sidebar, 355, 75);
  
  rss << totalredwins;
  strg = rss.str();
  char red[strg.length() + 1];
  strcpy(red, strg.c_str());
  
  Surf_Sidebar = CSurface::OnLoad("images/black.bmp");
  CSurface::OnDraw(Surf_Display, Surf_Sidebar, 475, 75);
  Surf_Sidebar = TTF_RenderText_Solid(font, red, textColor);
  CSurface::OnDraw(Surf_Display, Surf_Sidebar, 475, 75);

  minss << Chronotime / 60000; // Minutes
  strg = minss.str();
  strg += ":";
  secss << (Chronotime / 1000) % 60; // Seconds
  if ((Chronotime / 1000) % 60 < 10) { strg += "0"; }
  strg += secss.str();
  strg += ".";
  milliss << Chronotime % 1000; // Milliseconds
  if ((Chronotime % 1000) < 100) { strg += "0"; }
  if ((Chronotime % 1000) < 10) { strg += "0"; }
  strg += milliss.str();
  char time[strg.length() + 1];
  strcpy(time, strg.c_str());
  
  Surf_Sidebar = CSurface::OnLoad("images/black.bmp");;
  CSurface::OnDraw(Surf_Display, Surf_Sidebar, 390, 110);
  Surf_Sidebar = TTF_RenderText_Solid(font, time, textColor);
  CSurface::OnDraw(Surf_Display, Surf_Sidebar, 390, 110);

  Surf_Sidebar = TTF_RenderText_Solid(smallfont, 
                                      "Press p to pause", textColor);
  CSurface::OnDraw(Surf_Display, Surf_Sidebar, 340, 230);
  Surf_Sidebar = TTF_RenderText_Solid(smallfont,
                                      "Press r to reset board", textColor);
  CSurface::OnDraw(Surf_Display, Surf_Sidebar, 340, 250);
  Surf_Sidebar = TTF_RenderText_Solid(smallfont,"Press s to reset board",
                                      textColor);
  CSurface::OnDraw(Surf_Display, Surf_Sidebar, 340, 270);
  Surf_Sidebar = TTF_RenderText_Solid(smallfont,"and scoring", textColor);
  CSurface::OnDraw(Surf_Display, Surf_Sidebar, 340, 290);
  Surf_Sidebar = TTF_RenderText_Solid(smallfont,
                                      "Press q to quit", textColor);
  CSurface::OnDraw(Surf_Display, Surf_Sidebar, 340, 310);

  if ((! Gameon) && (! Somebodywon)) {
    Surf_Sidebar = TTF_RenderText_Solid(font, "PAUSE", textColor);
    CSurface::OnDraw(Surf_Display, Surf_Sidebar, 400, 140);
  } else {
    Surf_Sidebar = CSurface::OnLoad("images/black.bmp");
    CSurface::OnDraw(Surf_Display, Surf_Sidebar, 400, 140);
  }
  SDL_Flip(Surf_Display);
}
