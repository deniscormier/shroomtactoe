/*
 * Filename: CApp_OnLoop.cc
 * Author: Denis Cormier
 * Date of creation: 3 September 2011
 * Description:
 */

#include "CApp.h"

void CApp::OnLoop() {
  // Note: SDL_GetTicks() always goes up. It never stops
  
  if (Gameon && (! Somebodywon)) {
    // Game is playing
    Chronotime = SDL_GetTicks() - Gamestarttime;
  }
  if ((! Gameon) && (! Somebodywon)) { 
    // Game is paused and Chronotime stays the same
    Gamestarttime = SDL_GetTicks() - Chronotime;
  }
}
