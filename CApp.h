/*
 * Filename: CApp.h
 * Author: Denis Cormier
 * Date of creation: 3 September 2011
 * Description: Declarations
 */

#ifndef _CAPP_H_
#define _CAPP_H_

#include <SDL/SDL.h>
#include "CEvent.h"
#include "CSurface.h"
#include "SDL/SDL_ttf.h"

class CApp : public CEvent {
  private:
    bool Running;
    SDL_Surface* Surf_Display;
    SDL_Surface* Surf_Grid;
    SDL_Surface* Surf_X; // Represents the green shrooms
    SDL_Surface* Surf_O; // Represents the red shrooms
    SDL_Surface* Surf_Sidebar; // Displays information on the right
    TTF_Font *font;
    TTF_Font *smallfont;
    SDL_Color textColor;
    int Grid[9];
    enum Kind {
      GRID_TYPE_NONE = 0,
      GRID_TYPE_X,
      GRID_TYPE_O
    };
    int CurrentPlayer;
    long Gamestarttime;
    long Chronotime;
    bool Somebodywon;
    int totalgreenwins;
    int totalredwins;
    bool Gameon;
  public:
    CApp();
    int OnExecute();

    bool OnInit();
    void OnEvent(SDL_Event* Event);
    void OnExit();
    void OnLoop();
    void OnRender();
    void OnCleanup();
    
    void Reset();
    void SetCell(int ID, int Type);
    bool CheckWin(Kind xoro, char* color);
    void OnLButtonDown(int mX, int mY);
    void OnRButtonDown(int mX, int mY);
    void OnKeyDown(SDLKey key, SDLMod mod, Uint16 unicode);
};

#endif
