/*
 * Filename: CApp_OnInit.cc
 * Author: Denis Cormier
 * Date of creation: 3 September 2011
 * Description: Initialization
 */

#include "CApp.h"

bool CApp::OnInit() {
  if (SDL_Init(SDL_INIT_EVERYTHING) < 0 || TTF_Init() < 0) {
    return false;
  }

  if ((font = TTF_OpenFont("mask.ttf", 28)) == NULL) {
    return false;
  }

  if ((smallfont = TTF_OpenFont("mask.ttf", 18)) == NULL) {
    return false;
  }

  if ((Surf_Display = SDL_SetVideoMode(550, 330, 32, SDL_HWSURFACE |
                                       SDL_DOUBLEBUF)) == NULL) {
    return false;
  }

  if ((Surf_Grid = CSurface::OnLoad("images/grid.bmp")) == NULL) {
    return false;
  }
  
  if ((Surf_X = CSurface::OnLoad("images/angrygshroom.bmp")) == NULL) {
    return false;
  }

  if ((Surf_O = CSurface::OnLoad("images/angryrshroom.bmp")) == NULL) {
    return false;
  }

  SDL_WM_SetCaption("Tic-Tac-Shrooms", 0); // Set window title

  CSurface::Transparent(Surf_X, 255, 0, 255);
  CSurface::Transparent(Surf_O, 255, 0, 255);

  Reset();
  
  return true;
}
