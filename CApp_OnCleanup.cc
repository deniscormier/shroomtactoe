/*
 * Filename: CApp_OnCleanup.cc
 * Author: Denis Cormier
 * Date of creation: 3 September 2011
 * Description: Properly close windows and quit
 */

#include "CApp.h"

void CApp::OnCleanup() {
  SDL_FreeSurface(Surf_Grid);
  SDL_FreeSurface(Surf_X);
  SDL_FreeSurface(Surf_O);
  SDL_FreeSurface(Surf_Display);
  SDL_FreeSurface(Surf_Sidebar);
  TTF_CloseFont(font);
  TTF_Quit();
  SDL_Quit();
}
