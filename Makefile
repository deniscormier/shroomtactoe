CXX = g++ /usr/lib/libSDL_ttf.a
CXXFLAGS = -Wall -MMD -g -lSDL_ttf `sdl-config --cflags --libs`
OBJS = CApp.o CApp_OnCleanup.o CApp_OnEvent.o CApp_OnInit.o CApp_OnLoop.o CApp_OnRender.o CSurface.o CEvent.o
DEPENDS = ${OBJS:.o=.d} 
EXEC = shroomtactoe

# make shroomtactoe

$(EXEC): $(OBJS)
	$(CXX) $(CXXFLAGS) $(OBJS) -o $(EXEC)

# make run

run:
	./shroomtactoe

# make clean

clean:
	rm -f ${DEPENDS} ${OBJS} ${EXEC}

-include ${DEPENDS}
