/*
 * Filename: CApp.cc
 * Author: Denis Cormier
 * Date of creation: 3 September 2011
 * Description: Sets the stage for the entire program
 */

#include "CApp.h"

CApp::CApp() {
  CurrentPlayer = 0;
  Chronotime = 0;
  Somebodywon = 0;
  totalgreenwins = 0;
  totalredwins = 0;
  Gameon = 0;
  Gamestarttime = 0;
  Surf_Grid = NULL;
  Surf_X = NULL;
  Surf_O = NULL;
	Surf_Display = NULL;
  Surf_Sidebar = NULL;	
	textColor = {255, 255, 255};
  Running = true;
}

int CApp::OnExecute() {
  if (OnInit() == false) {
    return -1;
  }

  SDL_Event Event;

  while(Running) {
    while(SDL_PollEvent(&Event)) {
      OnEvent(&Event);
    }
    
    OnLoop();
    OnRender();
  }

  OnCleanup();

  return 0;
}

void CApp::Reset() {
  for(int i = 0; i < 9; i++) {
    Grid[i] = GRID_TYPE_NONE;
  }
  Chronotime = 0;
  Somebodywon = 0;
  Gameon = 1;
  Gamestarttime = SDL_GetTicks();
}

void CApp::SetCell(int ID, int Type) {
  if (ID < 0 || ID >= 9) return;
  if (Type < GRID_TYPE_NONE || Type > GRID_TYPE_O) return;

  Grid[ID] = Type;
}

int main(int argc, char* argv[]) {
  CApp theApp;

  return theApp.OnExecute();
}
