/*
 * Filename: CApp_OnEvent.cc
 * Author: Denis Cormier
 * Date of creation: 3 September 2011
 * Description:
 */

#include "CApp.h"
//#include <iostream>

using namespace std;

void CApp::OnEvent(SDL_Event* Event) {
  CEvent::OnEvent(Event);
}

void CApp::OnExit() {
  Running = false;
}

bool CApp::CheckWin(Kind xoro, char* color) {
  if (Grid[0] == xoro) {
    if (Grid[1] == xoro) {
      if (Grid[2] == xoro) {
        //cout << color << " player scores!" << endl;
        return true;
      }
    }

    if (Grid[3] == xoro) {
      if (Grid[6] == xoro) {
        //cout << color << " player scores!" << endl;
        return true;
      }
    }

    if (Grid[4] == xoro) {
      if (Grid[8] == xoro) {
        //cout << color << " player scores!" << endl;
        return true;
      } 
    }
  }

  if (Grid[1] == xoro) {
    if (Grid[4] == xoro) {
      if (Grid[7] == xoro) {
        //cout << color << " player scores!" << endl;
        return true;
      }
    }
  }

  if (Grid[2] == xoro) {
    if (Grid[5] == xoro) {
      if (Grid[8] == xoro) {
        //cout << color << " player scores!" << endl;
        return true;
      }
    }
            
    if (Grid[4] == xoro) {
      if (Grid[6] == xoro) {
        //cout << color << " player scores!" << endl;
        return true;
      } 
    }
  }

  if (Grid[3] == xoro) {
    if (Grid[4] == xoro) {
      if (Grid[5] == xoro) {
        //cout << color << " player scores!" << endl;
        return true;
      }
    }
  }

  if (Grid[6] == xoro) {
    if (Grid[7] == xoro) {
      if (Grid[8] == xoro) {
        //cout << color << " player scores!" << endl;
        return true;
      }
    }
  }
  return false;
}

void CApp::OnLButtonDown(int mX, int mY) {
  int ID = mX / 110;
  ID += ((mY / 110) * 3);
  
  if ( !(Gameon) || Somebodywon || Grid[ID] != GRID_TYPE_NONE || (mX >= 330) ) {
    return;
  }

  if (CurrentPlayer == 0) {
    SetCell(ID, GRID_TYPE_X);
    CurrentPlayer = 1;
  }
  else {
    SetCell(ID, GRID_TYPE_O);
    CurrentPlayer = 0;
  }

  if (CheckWin(GRID_TYPE_X, "Green")) {
    Somebodywon = 1;
    totalgreenwins++;
  }
  if (CheckWin(GRID_TYPE_O, "Red")) {
    Somebodywon = 1;
    totalredwins++;
  }
}

void CApp::OnRButtonDown(int mX, int mY) {
  //Reset();
}

void CApp::OnKeyDown(SDLKey sym, SDLMod mod, Uint16 unicode) {
  switch (sym) {
    case 'p': // Pause
      Gameon = !(Gameon);
      break;
    case 'q': // Quit
      OnExit();
      break;
    case 'r': // Reset board
      Reset();
      break;
    case 's': // Reset board and score
      Reset();
      totalgreenwins = 0;
      totalredwins = 0;
    default:
      break;
  }
}
